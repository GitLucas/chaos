using UnityEngine;
using UnityEngine.SceneManagement;

public class Restarter : MonoBehaviour
{
    [SerializeField] private GameObject pause;
    private void Awake()
    {
        DeathHandler.OnDeath += HandlePlayerDeath;
        gameObject.SetActive(false);
    }

    private void OnDestroy()
    {
        DeathHandler.OnDeath -= HandlePlayerDeath;
    }

    private void HandlePlayerDeath()
    {
        pause.SetActive(false);
        gameObject.SetActive(true);
    }

    private void Update()
    {
        if (!Input.GetButtonDown("Pause")) return;
        
        SceneManager.LoadScene("Game");
    }
}
