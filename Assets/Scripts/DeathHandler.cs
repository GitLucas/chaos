using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DeathHandler : MonoBehaviour
{
    [SerializeField] private GameObject[] hp;
    [Space]
    [SerializeField] private UnityEvent onDeath;
    public static event Action OnDeath;

    private int _hp = 2;
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (_hp < 0)
        {
            onDeath.Invoke();
            OnDeath?.Invoke();
            return;
        }
        
        hp[_hp--].SetActive(false);
        other.gameObject.SetActive(false);
    }
}
