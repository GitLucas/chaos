using UnityEngine;
using Random = System.Random;

public class StringCreator : MonoBehaviour
{
    private static char[] _values;
    private static Random _rng;
    private void Awake()
    {
        _rng = new Random();
        _values = new[] {'a', 'x', 'p', 'b'};
    }

    public static string GetRandomString(int length)
    {
        var s = "";
        for (var i = 0; i < length; i++)
            s += _values[_rng.Next(_values.Length)] + " ";
        return s;
    }
}
