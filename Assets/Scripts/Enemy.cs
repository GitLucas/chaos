using System;
using System.Text;
using TMPro;
using UnityEngine;
using Random = System.Random;

public class Enemy : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI text;
    [SerializeField] private Vector3 startingPosition;

    private string _target, _startingTarget;
    private int _index;

    private bool _ultra;
    
    private void OnEnable()
    {
        
        transform.position = startingPosition;
        _target = StringCreator.GetRandomString(4);
        _index = 0;

        if (new Random().NextDouble() < GameSettings.SuperEnemyChance)
            HandleSuperEnemy();
        else
            KeyboardHandler.KeyPressed += HandleKey;
    }

    private void HandleSuperEnemy()
    {
        KeyboardHandler.UltraKeyPressed += HandleUltraKey;
        KeyboardHandler.UltraEnabled += HandleUltraEnable;
        text.fontStyle = FontStyles.Underline;
        text.characterSpacing = -10;
        _startingTarget = _target;
    }

    private void HandleUltraEnable(bool obj)
    {
        _ultra = obj;
        if (_ultra) return;
        
        _index = 0;
        _target = _startingTarget;
    }

    private void LateUpdate()
    {
        text.text = _target;
    }

    private void HandleKey(char c)
    {
        if (_target[_index] == c)
        {
            var s = new StringBuilder(_target) {[_index] = '*'};
            _target = s.ToString();
            _index += 2;
        }
            
        if(_index >= _target.Length)
            gameObject.SetActive(false);
    }

    private void HandleUltraKey(char c)
    {
        if (!_ultra) return;
        
        HandleKey(c);
    }

    private void OnDisable()
    {
        KeyboardHandler.KeyPressed -= HandleKey;
        KeyboardHandler.UltraKeyPressed -= HandleUltraKey;
        KeyboardHandler.UltraEnabled -= HandleUltraEnable;
    }
}
