using System;
using UnityEngine;

public class KeyboardHandler : MonoBehaviour
{
    public static Action<char> KeyPressed;
    public static Action<char> UltraKeyPressed;
    public static Action<bool> UltraEnabled;
    public static Action<bool> Pause;

    private bool _paused = true;
    private bool _isUltraPressed;

    private void Start()
    {
        Pause?.Invoke(true);
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetButtonDown("Super"))
        {
            _isUltraPressed = true;
            UltraEnabled?.Invoke(true);
        }
        if(Input.GetButtonUp("Super"))
        {
            _isUltraPressed = false;
            UltraEnabled?.Invoke(false);
        }
        
        if (Input.GetButtonDown("Key"))
        {
            var c = char.MinValue;
            
            if(Input.GetKeyDown(KeyCode.A)) c = 'a';
            if(Input.GetKeyDown(KeyCode.X)) c = 'x';
            if(Input.GetKeyDown(KeyCode.P)) c = 'p';
            if(Input.GetKeyDown(KeyCode.B)) c = 'b';
            
            if(_isUltraPressed)
                UltraKeyPressed?.Invoke(c);
            else
                KeyPressed?.Invoke(c);
        }

        if (Input.GetButtonDown("Pause"))
        {
            _paused = !_paused;
            Pause?.Invoke(_paused);
        }
    }
}
