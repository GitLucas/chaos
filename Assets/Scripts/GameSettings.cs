using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameSettings : MonoBehaviour
{
    [SerializeField] private Stopwatch stopwatch;
    
    public static float TimeBetweenEnemies { get; private set; }
    public static float SuperEnemyChance { get; private set; }
    public static float EnemySpeed { get; private set; }
    
    public static bool GamePaused { get; private set; }

    private float _spawnRateUpgrade = 10f;
    private float _superEnemyUpgrade = 5f;
    private float _speedUpgrade = 7f;

    private void Awake()
    {
        SceneManager.LoadScene("Menu", LoadSceneMode.Additive);
        EnemySpeed = .03125f;
        TimeBetweenEnemies = 3f;
        SuperEnemyChance = .03125f;
        KeyboardHandler.Pause += Pause;
    }

    private void Update()
    {
        if (GamePaused) return;
        
        var t = stopwatch.GetRawElapsedTime();

        if (t > _spawnRateUpgrade)
        {
            TimeBetweenEnemies -= .25f;
            _spawnRateUpgrade = t + 10f;
        }

        if (t > _superEnemyUpgrade)
        {
            SuperEnemyChance *= 1.5f;
            _superEnemyUpgrade = t + 7f;
        }

        if (t > _speedUpgrade)
        {
            EnemySpeed += 0.015625f;
            _speedUpgrade = t + 9f;
        }
        
    }

    private void Pause(bool obj)
    {
        GamePaused = obj;
    }

    private void OnDestroy()
    {
        KeyboardHandler.Pause -= Pause;
    }
}
