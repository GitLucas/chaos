//Written by Daniel Keele - 6/6/2019

using System;
using UnityEngine;

public class Stopwatch : MonoBehaviour
{
    private void Awake()
    {
        KeyboardHandler.Pause += OnGamePaused;
    }

    private void Start() => Begin();

    private void OnGamePaused(bool gamePaused)
    {
        if(gamePaused) 
            Pause();
        else 
            Unpause();
    }

    private float _elapsedRunningTime;
    private float _runningStartTime;
    private float _pauseStartTime;
    private float _elapsedPausedTime;
    private float _totalElapsedPausedTime;
    private bool _running;
    private bool _paused;

    private void Update()
    {
        if (_running)
        {
            _elapsedRunningTime = Time.time - _runningStartTime - _totalElapsedPausedTime;
        }
        else if (_paused)
        {
            _elapsedPausedTime = Time.time - _pauseStartTime;
        }
    }

    private void Begin()
    {
        if (_running || _paused) return;
        
        _runningStartTime = Time.time;
        _running = true;
    }

    public void Pause()
    {
        if (_running && !_paused)
        {
            _running = false;
            _pauseStartTime = Time.time;
            _paused = true;
        }
    }

    public void Unpause()
    {
        if (!_running && _paused)
        {
            _totalElapsedPausedTime += _elapsedPausedTime;
            _running = true;
            _paused = false;
        }
    }

    private void OnDestroy()
    {
        KeyboardHandler.Pause -= OnGamePaused;
    }

    public float GetRawElapsedTime()
    {
        return _elapsedRunningTime;
    }
}