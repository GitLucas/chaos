using System;
using UnityEngine;

public class MenuLoader : MonoBehaviour
{
    
    private void Start()
    {
        KeyboardHandler.Pause += Pause;
    }

    private void Pause(bool obj)
    {
        gameObject.SetActive(obj);
    }

    private void OnDestroy()
    {
        KeyboardHandler.Pause -= Pause;
    }
}
