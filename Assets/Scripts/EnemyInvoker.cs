using System;
using UnityEngine;

public class EnemyInvoker : MonoBehaviour
{
    [Space] 
    [SerializeField] private Enemy[] enemies;
    [Space] 
    [SerializeField] private Stopwatch stopwatch;

    private float _nextSpawn;
    private Pool<Enemy> _pool;

    private void Start()
    {
        _nextSpawn = GameSettings.TimeBetweenEnemies;
        _pool = new Pool<Enemy>(enemies);
    }

    private void Update()
    {
        if (GameSettings.GamePaused) return;
        
        if (stopwatch.GetRawElapsedTime() < _nextSpawn) return;

        _nextSpawn = stopwatch.GetRawElapsedTime() + GameSettings.TimeBetweenEnemies;
        _pool.Get().gameObject.SetActive(true);
    }
}
