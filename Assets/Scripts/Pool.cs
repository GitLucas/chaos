using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = System.Random;

[Serializable]
public class Pool<T> where T : Component
{
    private readonly T[] _prefabs;
    private HashSet<int> _spawned;
    private Random _rng;

    [HideInInspector]
    public Func<Component, bool> CanBeUsedCondition = x => { return !x.gameObject.activeInHierarchy; };


    public Pool(T[] prefabs)
    {
        _prefabs = prefabs;
        _rng = new Random();
        _spawned = new HashSet<int>();

        SpawnPrefabs();
    }

    private void SpawnPrefabs()
    {
        foreach (var t in _prefabs)
        {
            var newObject = Object.Instantiate(t);
            PooledObjects.Add(newObject);
            newObject.gameObject.SetActive(false);
        }
    }

    public List<T> PooledObjects { get; protected set; } = new List<T>();

    private int _tries;

    public T Get()
    {
        _tries = 0;
        T o;
        do
        {
            o = PooledObjects[_rng.Next(PooledObjects.Count)];
            _tries++;
        } while (!CanBeUsedCondition(o) && _tries < PooledObjects.Count);
        
        return CanBeUsedCondition(o) ? o : CreateNew();
    }

    private T CreateNew()
    {
        var i = _rng.Next(_prefabs.Length);
        
        var newObject = Object.Instantiate(_prefabs[i]);
        _spawned.Add(i);
        PooledObjects.Add(newObject);
        return newObject;
    }
}