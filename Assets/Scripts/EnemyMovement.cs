using System;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    private Vector3 _playerPosition;
        
    private void Start()
    {
        _playerPosition = GameObject.FindWithTag("Player").transform.position;
    }

    private void FixedUpdate()
    {
        if (GameSettings.GamePaused) return;
        
        transform.position = Vector3.MoveTowards(transform.position, _playerPosition, GameSettings.EnemySpeed);
    }
}
